<div align="center">
<p align="center"><img src="https://gitlab.com/librehealth/incubating-projects/mhbs/mHBS-trainer/uploads/f74b5d60bd6501cf9aeb1ff39fb3941a/app_logo.png" width="150"></p>
<h2 align="center"><b>mobile-Helping Babies Breath (Trainer App)</b></h2>
<h4><a href="https://gitlab.com/librehealth/incubating-projects/mhbs/mHBS-trainer/-/jobs/artifacts/master/raw/platforms/android/app/build/outputs/apk/debug/app-debug.apk?job=assembleDebug">Download Android Application (Link)</a></h4>
</div>


# mHBS - Mobile Helping Babies Breathe
The mobile Helping Babies Survive (mHBS) app is a digital health tool, developed by collaborators at Indiana University (School of Medicine and School of Informatics and Computing), which consists of 3 separate apps, serving functions related to data collection and reporting, access to educational resources, including other apps and data visualization. The mHBS apps are powered by the District Health Information Software ([DHIS 2](https://dhis2.org)) in the back-end for data storage and tracking of health workers.

# mHBS Training App
The mHBS Training App is a community health worker training app focused on training for neonatal resuscitation, through videos, guide and other kinds of training material. As part of the [mHBS suite of apps](https://neoinfo.iu.edu/services/mobile-helping-babies-breathe/), after community health workers have been trained, then can test and their follow-up scores are tracked through the [mHBS tracker](https://gitlab.com/librehealth/incubating-projects/mhbs/mHBS-tracker), a fork of the DHIS2 tracker app.

## Tech Stack
* Node
* Framework 7
* Cordova 11.0
* Java 11+

## Usage
### 1. Clone the Repository
```
git clone https://gitlab.com/librehealth/incubating-projects/mhbs/mHBS-trainer.git
```
### 2. Add OS specific platforms

For Android:
Go to the downloaded repository folder and run:
```
cordova platform add android
```

### 3. Build the app
(plugins will be automatically built)
```
npm i xml2js
```

### 4. Build the app
(plugins will be automatically built)
```
cordova build
```

### 5. Run or emulate the app
To run the app on android (... assuming that an Android device is connected)
```
cordova run android
```

OR to emulate on the Android emulator (... assuming that the Android SDK/Android Emulator has been installed)
```
cordova emulate android
```

## Features
### 1. Offline support and sync facility for Media Page

 - List of documents on the media page is in synchronisation with the resources uploaded on the dhis2 and also available in offline mode.

 - For every document when the user clicks :

     If : It is already downloaded it will be played using the fileopener2 Cordova plugin

     and If not then : Based on internet connection it will get downloaded and saved to File System of the local device, Corresponding file
                       location will be updated to SQLite-DB as well.

 - A separate icon is also placed on every document to show its sync status.

 - On ongoing sync, a progress bar is visible on the document.

 * For better understanding of all logic and implementation details please check [this](https://bhavesh3005sharma-gsoc21.blogspot.com/2021/07/fourth-week-of-coding-26-june-2july.html).


### 2. How to upload the resources and how to access them?

 - All type of resources are supported means you can upload pdf, video, images, documents, xls, etc. all these documents will be uploaded through
   the dhis2 resources and will be saved as a blob object.

 - Media Page of the tracker app is in full syncronization with the resources means if you edit the name of any file or remove it or add new file it
   will get reflected to the media Page but media files are not in sync to save the user's data.

 - If you want to update a document file then it is must to remove the old file and then upload the new file as new documents in dhis2, so that users
   can see these changes and can download the latest content.

 - Media files will be accessible from trainer app and trainee will be able to see all those resources whose supported reader app is present in this
   device. User must have a supported third party app on this device to read the document.

 * For better understanding please check following resources :

   [Demonstration_of_Uploading_resources_on_dhis2](/uploads/c761d45b6871155e19418ad73282fa64/Demonstration_of_Uploading_resources_on_dhis2.mp4)

   [Demonstartion_of_accessing_resources_on_trainer_app_through_tracker_app](/uploads/71094693258e348296d55aba72b2bded/Media-Page__1_.mp4)


 ### 3. App Usage tracking system
 - Usage of the app can be tracked over dhis2.
 - App Usage ( Page Visits and Time Spent for each page ) will be stored locally in SQL DB.
 - When usage crosses a threshold limit then this usage will be updated to the user's data store at dhis2 by API calls.
 - And the local SQL DB reset to store new usages data.
 - To store app usage for every page, a random and fixed id is given which works as a key for a page in the user's datastore.
 - Usages are stored in the "trainer-app-usage" namespace with a random and fixed key for everypage.

 - Schema of JSON stored by keys in data store :
```{
"page_name": "homepage",
"page_visits": 4,
"page_time_spent": 2.5443666666666664
}```


